<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Auth');
	}
	
	public function index()
	{
    $roles = $this->auth->getRoles();

		$data = [
			'roles' => $roles
		];
		$this->load->view('auth/roles/list', $data);
	}

	public function create() {
		$this->load->library('Form_validation');
	
		$this->form_validation->set_rules('role', 'Role Name', 'trim|required');
		
		if ($this->form_validation->run() === TRUE) {
			$post = $this->input->post();

			if ($this->auth->insertRole($post['role'], $post['perm'])) {
				redirect(base_url());
			}

			echo validation_errors();
		} else {
			$data = [
				'permissions' => $this->auth->getPermissions()
			];
			
			$this->load->view('auth/roles/add', $data);
		}
	}

}

/* End of file Role.php */
