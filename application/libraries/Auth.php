<?php
class Auth
{
  protected $CI;
  public    $user;
  public    $userdata;
  private   $roles;
  protected $permissions;

  public function __construct()
  {
    $this->CI =& get_instance();

    $this->CI->load->model('Role_model');
    $this->CI->load->model('Permission_model');
    $this->CI->load->model('Privileged_model');
    
    $this->user         = array();
    $this->roles        = array();
    $this->permissions  = array();
  }

  public function getUsers() {
    $users = $this->CI->Privileged_model->getUsers();
    $userdata = array();
    foreach ($users as $k => $user) {
      $userdata[] = $this->getByEmail($user['email']);
    }

    return $userdata;
  }

  public function getRoles() {
    $roles    = $this->CI->Role_model->getRoles();
    $roledata = array();
    foreach ($roles as $k => $role) {
      $roledata[$k]          = $role;
      $roledata[$k]['roles'] = $this->getRolePerms($role['role_id']);
    }

    return $roledata;
  }

  // return a role object with associated permissions
  public function getRolePerms($role_id) {
    $permissions = $this->CI->Role_model->getRolePermissions($role_id);

    foreach ($permissions as $k => $v) {
      $this->permissions[$v['perm_desc']] = true;
    }

    return $this->permissions;
  }

  public function getPermissions() {
    $permissions = $this->CI->Permission_model->getPermissions();

    return $permissions;
  }
  
  // check if a permission is set
  public function hasPerm($permission) {
    return isset($this->permissions[$permission]);
  }

  // override User method
  public function getByEmail($email) {
    $user = $this->CI->Privileged_model->getByEmail($email);

    if (!empty($user)) {
      $this->userdata['fields']['id']           = $user['id'];
      $this->userdata['fields']['email']        = $email;
      $this->userdata['fields']['display_name'] = $user['display_name'];
      $this->userdata['fields']['password']     = $user['password'];
      $this->userdata['roles']                  = $this->_initRoles($user['id']);

      return $this->userdata;
    }
    
    return false;
  }

  // populate roles with their associated permissions
  private function _initRoles($user_id) {
    $roles = $this->CI->Privileged_model->getRolesAndPermissions($user_id);
    
    foreach ($roles as $k => $v) {
      $this->roles[$v['role_name']] = $this->getRolePerms($v['role_id']);
    }

    return $this->roles;
  }

  // check if user has a specific privilege
  public function hasPrivilege($perm) {
    foreach ($this->roles as $role) {
      if ($this->hasPerm($perm)) {
        return $this->hasPerm($perm);
      }
    }
    return false;
  }

  // check if a user has a specific role
  public function hasRole($role_name) {
    return isset($this->roles[$role_name]);
  }

  public function insertRole($role, $perms) {
    if ($role_id = $this->CI->Role_model->insertRole($role) ) {
      foreach ($perms as $key => $perm) {
        $this->CI->Privileged_model->insertPerm($role_id, $perm);
      }

      return true;
    }

    return false;
  }
  
}
