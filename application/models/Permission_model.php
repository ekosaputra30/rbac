<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_model extends CI_Model {

  public function getPermissions() {
    $return = $this->db->get('permissions');
    return $return->result_array();
  }

}

/* End of file Permission_model.php */
