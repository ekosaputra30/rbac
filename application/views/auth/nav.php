<nav class="nav nav-pills justify-content-center">
  <a class="nav-link <?php echo (($this->uri->segment(1) == '') || ($this->uri->segment(1) == 'user')) ? 'active' : false ?>" href="<?php echo site_url('user') ?>">User</a>
  <a class="nav-link <?php echo ($this->uri->segment(1) == 'role') ? 'active' : false ?>" href="<?php echo site_url('role') ?>">Roles</a>
</nav>