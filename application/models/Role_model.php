<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {

  public function getRoles() {
    $return = $this->db->get('roles');

    return $return->result_array();
  }

  public function getRolePermissions($role_id) {
    $return = [];

    $query = $this->db->query(
      "SELECT t2.perm_desc FROM role_perm as t1
      JOIN permissions as t2 ON t1.perm_id = t2.perm_id
      WHERE t1.role_id = $role_id"
    );

    return $query->result_array();
  }

  public function insertRole($role_name) {
    if($this->db->insert('roles', ['role_name' => $role_name])) return $this->db->insert_id();
    return false;
  }

  public function insertUserRoles($user_id, $role_id) {
    if($this->db->insert('user_role', ['user_id' => $user_id, 'role_id' => $role_id])) return $this->db->insert_id();
    return false;
  }

  public function deleteRoles($role_id) {
    return $this->db->query(
      "DELETE t1, t2, t3 FROM roles as t1
      JOIN user_role as t2 on t1.role_id = t2.role_id
      JOIN role_perm as t3 on t1.role_id = t3.role_id
      WHERE t1.role_id = $role_id"
    );
  }

  public function deleteUserRoles($user_id) {
    return $this->db->delete('user_role', ['user_id' => $user_id]);    
  }

}

/* End of file Role_model.php */
