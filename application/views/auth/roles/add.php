<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="wrapper">
            <?php echo $this->load->view('auth/nav', [], TRUE); ?>
            <h1>Simple RBAC &mdash; add role</h1>
            <?php if ( validation_errors() !== null ){
              echo validation_errors();
            }?>
            <form action="" method="post" class="">
              <div class="row">
                <div class="col-6">
                  <div class="form-group row">
                    <label for="role" class="col-sm-4 col-form-label">Role Name</label>
                    <div class="col-sm-8">
                      <input type="text" name="role" id="role" class="form-control" placeholder="ex: 'admin' or 'developer" aria-describedby="help-role">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="perm" class="col-sm-4 col-form-label">Permissions</label>
                    <div class="col-sm-8">
                      <?php foreach($permissions as $k => $perm):?>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="perm[]" id="perm-<?php echo $k+1 ?>" value="<?php echo $perm['perm_id'] ?>"> <?php echo $perm['perm_desc'] ?>
                          </label>
                        </div>
                      <?php endforeach;?>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8 offset-sm-4">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>