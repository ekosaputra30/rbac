<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="wrapper">
            <?php echo $this->load->view('auth/nav', [], TRUE); ?>
            <h1>Simple RBAC &mdash; Roles</h1>
            <div class="row">
              <div class="col col-sm-12">
                <a href="<?php echo site_url('role/create') ?>" class="btn btn-primary">Add Role</a>
              </div>
            </div>
            <div class="row">
              <div class="col col-sm-12">
                <table class="table table-hovered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Role</th>
                      <th>Permissions</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($roles as $k => $role):?>
                      <tr>
                        <td scope="row"><?php echo $k+1 ?></td>
                        <td><?php echo ($role['role_name']) ?></td>
                        <td>
                          <?php foreach($role['roles'] as $j => $role2):?>
                            <button type="button" class="btn btn-secondary">
                              <?php echo $j ?>
                            </button>
                          <?php endforeach;?>
                        </td>
                        <td>
                          <div class="btn-group">
                            <a href="<?php echo site_url('role/edit/'.$role['role_id']) ?>" class="btn btn-info">edit</a>
                            <a href="<?php echo site_url('role/show/'.$role['role_id']) ?>" class="btn btn-default">view</a>
                            <a href="<?php echo site_url('role/delete/'.$role['role_id']) ?>" class="btn btn-danger" onclick="return confirm('are you sure?')">delete</a>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>