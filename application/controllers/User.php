<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Auth');
	}
	
	public function index()
	{
		$users = $this->auth->getUsers();

		$data = [
			'users' => $users
		];
		$this->load->view('auth/users/list', $data);
	}

	public function create() {
		$this->load->library('Form_validation');
	
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		
		if ($this->form_validation->run() === TRUE) {
			$post = $this->input->post();
			
			echo "<pre>";
			print_r ($post);
			echo "</pre>";

			echo validation_errors();
		} else {
			$data = [
				'permissions' => $this->auth->getPermissions()
			];
			
			$this->load->view('auth/role/add', $data);
		}
	}
}
