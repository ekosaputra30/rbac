<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Privileged_model extends CI_Model {
  
  public function getUsers() {
    $return = $this->db->query("SELECT * FROM users ORDER BY id DESC");

    return $return->result_array();
  }

  public function getByEmail($email) {
    $return = $this->db->query("SELECT * FROM users WHERE email = '$email'");

    return $return->row_array();
  }

  public function getRolesAndPermissions($user_id) {
    $return = $this->db->query(
      "SELECT t1.role_id, t2.role_name FROM user_role as t1
      JOIN roles as t2 ON t1.role_id = t2.role_id
      WHERE t1.user_id = $user_id"
    );

    return $return->result_array();
  }

  public function insertPerm($role_id, $perm_id) {
    if ($this->db->insert('role_perm', ['role_id' => $role_id, 'perm_id' => $perm_id])) return $this->db->insert_id();
    return false;
  }

  public function deletePerms() {
    return $this->db->truncate('role_perm');
  }
}

/* End of file Privileged_model.php */
